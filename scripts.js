var menuBtn = document.querySelector('.menu-icon');
var menu = document.querySelector('.navigation ul');
var body = document.querySelector('body');

menuBtn.addEventListener('click', function() {
    if (menu.classList.contains('show')) {
        menu.classList.remove('show');
    } else {
        menu.classList.add('show');
    }
});

